
FROM python:3.8

COPY requirements.txt /
RUN pip3 install -r requirements.txt

ARG ARQ_ADMINS

WORKDIR app
COPY app.py app.py

CMD [ "gunicorn", "--workers=4", "--threads=1", "-b 0.0.0.0:5000", "app:app"]