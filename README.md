# ArQ Auth

`arq-api` is an Authentication solution using webhooks for the [ArQ](https://gitlab.com/inqtel/arq) project written in Python using Flask.

<figure>
<a href="https://hasura.io/docs/1.0/graphql/core/auth/authentication/webhook.html">
<img src="https://hasura.io/docs/1.0/_images/webhook-auth1.png" title="Webhook Image from Hasura Docs">
</a>
<figcaption>Diagram by <a href="https://hasura.io/">hasura.io</a></figcaption>
</figure>


## Configure Hasura

Configure Hasura to use a webhook:

1. Set HASURA_GRAPHQL_ADMIN_SECRET
2. Ensure  `X-Hasura-Admin-Secret` is not sent in the request
3. Set HASURA_GRAPHQL_AUTH_HOOK_MODE to use POST. (Hasura sends all client headers in the payload when HASURA_GRAPHQL_AUTH_HOOK_MODE is set to use POST).

Add these environment variables to your `hasura` service in docker-compose.yml:

```console
HASURA_GRAPHQL_ADMIN_SECRET: $HASURA_GRAPHQL_ADMIN_SECRET
HASURA_GRAPHQL_AUTH_HOOK: http://webhook:5000/auth-webhook
HASURA_GRAPHQL_AUTH_HOOK_MODE: POST
```

## Run Locally with Flask

```console
cp .env-example .env
pip3 install -r requirements.txt
pip install python-dotenv
flask run
```

Webhook running at http://localhost:5000/auth-webhook

Use [Postman](https://www.postman.com) to make a POST request.

## Run as a Service in ArQ-API

Add webhook service to docker-compose.yml using image built and stored in this [repo](https://gitlab.com/inqtel/arq-auth/container_registry).

```console
  webhook:
    image: registry.gitlab.com/inqtel/arq-auth
    container_name: webhook
    restart: always
    ports:
      - "5000:5000"
    environment:
      ARQ_ADMINS: $ARQ_ADMINS
```

Add ARQ_ADMINS to arq-api .env.

```console
ARQ_ADMINS=mcurie@example.com,efranklin@example.com
```

Run the arq-api Startup Script

```console
./startup.sh
```

Start [arq-client](https://gitlab.com/inqtel/arq-client#developer-quickstart-mac-osx)

When switching between users, only admins listed in the arq-api .env file will have additional admin privileges. Fork this project and add your own custom logic to implement your authentication needs.


## References
Thanks to Hasura for providing an [auth-webhook boilerplate](https://github.com/hasura/graphql-engine/tree/master/community/boilerplates/auth-webhooks/python-flask) which was used as a guide for this solution.