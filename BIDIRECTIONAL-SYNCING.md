# Bidirectional Syncing
ArQ-Auth is using the power of Gitlab Continuous Integration (CI) to automate the syncing of its internal and open source repositories.

<strong>Why not sunset the internal codebase and move all development to the open source repositories?</strong>

Because ArQ-Auth also utlizes Gitlab's awesome Continous Deployment (CD) to auto deploy ArQ-Auth into its internal infrastructure.

What is important to note is that contributing your code to ArQ-Auth's open source `production` branches will make its way to our internal `production` branches, and vice versa, for those contributing from the inside.


## Protected Branches
Internally, our `development`, `production` and `public-production` branches are protected branches. Our open source `production` branch is also protected. 

Protected branches are the stable branches of a repository.

That means that:
1. New code must be reviewed and approved via Merge Requests before code can be merged into Protected Branches
2. Protected Branches can not be overwritten with force pushes
3. Protected Branches can not be deleted
4. Protected Branches can not be created except by Maintainers

## Code Flow

<img src="https://gitlab.com/inqtel/arq-api/uploads/c77812e0c388c491ccec08741e01c1a6/git-flow.png" alt="Code Flow">



### Open Source Code Flow
Your code contributions will start with a branch off of `production`. Once you have solved a bug, enhanced a feature, or updated documentation, push your working branch up and submit a Merge Request. A member of the development team will be notified to review your code. 

 
### Internal Code Flow
Code contributions start with branching off of `development`. Push your working branch up and submit a Merge Request. A member of the development team will be notified to review your code.

Internal code contributions are the siphon to get code in. When a developer commits and pushes code up to the internal repository, it kicks off a gitlab job to pull code in.

This was done for 2 reasons:

1. Historically, new code is pushed to the internal repo many times a day
2. The sync had to be intiated internally

Continuosly contributing code and continuosly deploying code to production, gets code pulled in and pushed out.

## Internal Merge Requests

When code is merged into `public-production`, a merge request is created from `public-production` to `development`. A developer must validate all code coming into the internal repo.

When code is deployed to `production`, a merge request is created from `production` to `public-production`. A developer must validate all code going out to the open source repo. A manual job named `public-production-manual-push` appears after approving and merging this MR. A developer must manually run this job to push the code out.

<strong>Do you want to open source your internal code and maintain and sync two repositories?</strong>

Checkout these three jobs in our [gitlab-ci.yml](https://gitlab.com/inqtel/arq-api/-/blob/production/.gitlab-ci.yml) and tweak them to do the same for you:

* public-production-manual-push

* open-source-production-auto-fetch

* public-production-auto-merge-request