from flask import Flask, request, abort
import os

app = Flask(__name__)

@app.route('/')
def status():
    return 'webhook is running'

@app.route('/auth-webhook', methods=['POST'])
def webhook():
    if request.method == 'POST':
        admin_list = os.environ['ARQ_ADMINS'].split (",")
        if request.json is not None:
            ## sets X-Hasura-Role header to api-admin if remote user in admin list
            if request.json['headers']['X-Hasura-Remote-User'] in admin_list:
                request.json['headers']['X-Hasura-Role'] = 'api-admin'
            return request.json['headers'], 200
        else:
            return '', 200
    else:
        abort(401)


if __name__ == '__main__':
    app.run()